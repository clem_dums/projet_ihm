import { Component, OnInit } from '@angular/core'
import { PanierServiceService } from '../services/panier-service.service'
import { MovieResponse } from '../tmdb-data/Movie'

@Component({
  selector: 'app-valide-panier',
  templateUrl: './valide-panier.component.html',
  styleUrls: ['./valide-panier.component.scss'],
})
export class ValidePanierComponent implements OnInit {
  constructor(private p: PanierServiceService) {}

  ngOnInit(): void {}

  commandeEnvoye(): boolean {
    return this.p.commandeEnvoye()
  }

  getMoviePanier(): MovieResponse[] {
    return this.p.getMoviePanier()
  }

  getCout(): number {
    return Math.round(this.p.getCout() * 100) / 100
  }

  taillePanierClient() {
    return this.p.taillePanier()
  }

  getNomFilm(film: MovieResponse) {
    return film.title
  }

  adresseSaisi(): boolean {
    return this.p.adresseSaisi()
  }

  confirmePanier() {
    console.log(this.p.getMoviePanier())
    this.p.sendPanierToServeur()
  }
}
