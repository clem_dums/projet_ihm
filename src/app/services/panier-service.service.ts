import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http'
import { LoginService } from './login.service'
import { MovieResponse } from '../tmdb-data/Movie'
import { Router } from '@angular/router'
import { Adresse } from 'src/types'

@Injectable({
  providedIn: 'root',
})
export class PanierServiceService {
  private cmdEnvoye: boolean = false
  private cmdEnvoye$: BehaviorSubject<boolean> = new BehaviorSubject(
    this.cmdEnvoye
  )
  private ListeFilmCommande: MovieResponse[]
  private cout: number = 0
  private entrainDeSaisir: boolean
  public cout$: BehaviorSubject<number> = new BehaviorSubject(this.cout)
  private Adr: Adresse

  constructor(
    private http: HttpClient,
    private log: LoginService,
    private router: Router
  ) {
    this.ListeFilmCommande = []
    this.entrainDeSaisir = false

    this.cout$.subscribe((c) => {
      this.cout = c
    })
    this.cmdEnvoye$.next(false)
    this.cmdEnvoye$.subscribe((cmd) => {
      this.cmdEnvoye = cmd
    })
    this.setAdresse({ rue: '', ville: '', tel: '', pays: '', codeP: '' })
  }

  setAdresse(adr: Adresse) {
    this.Adr = adr
  }

  AdresseServeur() {
    return this.Adr.rue + ', ' + this.Adr.codeP + ', ' + this.Adr.ville
  }

  getAdresse(): Adresse {
    return this.Adr
  }

  ajoutFilmPanier(film: MovieResponse) {
    this.ListeFilmCommande.push(film)
  }

  enleveFilmPanier(index_remove: number) {
    this.ListeFilmCommande.splice(index_remove, 1)
  }

  getCout(): number {
    return this.cout
  }

  getMoviePanier(): MovieResponse[] {
    return this.ListeFilmCommande
  }

  setEntrainDeSaisir(b: boolean) {
    this.entrainDeSaisir = b
  }
  getEntrainDeSaisir(): boolean {
    return this.entrainDeSaisir
  }

  adresseSaisi(): boolean {
    return this.Adr.codeP != '' && this.Adr.ville != '' && this.Adr.rue != ''
  }

  taillePanier() {
    return this.ListeFilmCommande.length
  }

  emitCMDSubject(boo: boolean) {
    this.cmdEnvoye$.next(boo)
  }

  //Renvoie la liste des id des films présent dans le panier
  listeIdFilms(): string {
    let idTab: string = ''
    this.ListeFilmCommande.forEach((film: MovieResponse) => {
      idTab = idTab + film.id.toString() + ' - '
    })
    return idTab
  }

  resetPanier() {
    this.ListeFilmCommande = []
  }

  sendPanierToServeur() {
    let IdClient: string = this.log.getIdClient()
    let IdsFilms: string = this.listeIdFilms()
    let Prix: string = this.cout.toString()

    let Adresse: string = this.AdresseServeur()
    const repServeur = this.POST('/api/commande', {
      idUsers: IdClient,
      idFilms: IdsFilms,
      prixCommande: Prix,
      adresseLivraison: Adresse,
    })
    this.emitCMDSubject(true)
    setTimeout((_) => {
      this.resetPanier()
      this.emitCMDSubject(false)
      this.router.navigate(['/acceuil'])
    }, 5000)
  }

  commandeEnvoye(): boolean {
    return this.cmdEnvoye
  }

  private POST(
    url: string,
    params: { [key: string]: string }
  ): Promise<HttpResponse<string>> {
    const P = new HttpParams({ fromObject: params })
    return this.http
      .post(url, P, {
        observe: 'response',
        responseType: 'text',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
      })
      .toPromise()
  }

  panierNonVide(): boolean {
    return this.ListeFilmCommande.length != 0
  }
}
