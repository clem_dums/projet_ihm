import { User, auth } from 'firebase'
import { AngularFireAuth } from '@angular/fire/auth'
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Users, Utilisateur, Commande } from 'src/types'
import { Router } from '@angular/router'

@Injectable()
export class LoginService {
  private validePanier: boolean = false
  user: User
  infoUser: Utilisateur

  constructor(
    private auth: AngularFireAuth,
    private http: HttpClient,
    private router: Router
  ) {
    this.resetInfoUser()
    auth.user.subscribe(async (u: User) => {
      this.user = u
      // On contacte le serveur métier pour l'informer si un nouvel utilisateur existe :
      if (u !== null) {
        this.resetInfoUser()
        this.setInfoUser(u)
        // this.setInfoUser(reponseServeur.body)
        // let utilisateur: Users = {
        //   idClient: u.uid,
        //   nom: u.displayName.split(' ')[1],
        //   prenom: u.displayName.split(' ')[0],
        //   photoURL: u.photoURL,
        //   mail: u.email,
        //   tel: u.phoneNumber,
        // }
        // // }
      }
    })
  }

  setInfoUser(user: User) {
    this.infoUser.idClient = user.uid
    this.infoUser.prenom = user.displayName.split(' ')[0]
    this.infoUser.nom = user.displayName.split(' ')[1]
    this.infoUser.photoURL = user.photoURL
    this.infoUser.mail = user.email
    this.infoUser.tel = user.phoneNumber

    // setInfoUser(text) {
    // let parser = new DOMParser()
    // let xmlClient = parser.parseFromString(text, 'application/xml')
    // this.infoUser.nom = xmlClient.getElementsByTagName('Nom').item(0).innerHTML
    // this.infoUser.photoURL = xmlClient
    //   .getElementsByTagName('Photo')
    //   .item(0).innerHTML
    // this.infoUser.mail = xmlClient
    //   .getElementsByTagName('Mail')
    //   .item(0).innerHTML
    // this.infoUser.tel = xmlClient.getElementsByTagName('Tel').item(0).innerHTML
    // if (this.infoUser.tel === null) {
    //   this.infoUser.tel = ' '
    // }
  }

  isAuth() {
    return +this.infoUser.idClient != -1
  }

  getIdClient(): string {
    return this.infoUser.idClient
  }
  getNomUtilisateur(): string {
    return this.infoUser.nom
  }
  getPrenomUtilisateur(): string {
    return this.infoUser.prenom
  }
  getUrlPhotoProfil(): string {
    return this.infoUser.photoURL
  }
  getEmail() {
    return this.infoUser.mail
  }
  getPhoneNumber() {
    return this.infoUser.tel
  }

  setTrueValideCommande() {
    this.validePanier = true
  }
  setFalseValideCommande() {
    this.validePanier = false
  }

  loginGoogle() {
    this.auth.signInWithPopup(new auth.GoogleAuthProvider())
  }
  signOutUser() {
    this.auth.signOut()
    this.resetInfoUser()
  }
  resetInfoUser() {
    this.infoUser = {
      idClient: '-1',
      nom: '',
      prenom: '',
      photoURL: '',
      mail: '',
      tel: '',
      nbTotalCommande: '',
      derniereCommande: {
        date: '',
        idCommande: '',
        idsFilms: [''], //Liste d'id de Films
        prix: '', //Cout de la commande
        adresse: '', //Adrersse de livraison
      },
    }
  }

  private POST(
    url: string,
    params: { [key: string]: string }
  ): Promise<HttpResponse<string>> {
    const P = new HttpParams({ fromObject: params })
    return this.http
      .post(url, P, {
        observe: 'response',
        responseType: 'text',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
      })
      .toPromise()
  }
}
