import { Component, OnInit } from '@angular/core'
import { PanierServiceService } from '../services/panier-service.service'
import { MenuCinemaView } from '../services/menuCinemaView.service'
import { MovieResponse } from '../tmdb-data/Movie'

@Component({
  selector: 'app-panier-client',
  templateUrl: './panier-client.component.html',
  styleUrls: ['./panier-client.component.scss'],
})
export class PanierClientComponent implements OnInit {
  constructor(private p: PanierServiceService, private mcv: MenuCinemaView) {}

  enleveFilm(i: number) {
    this.p.enleveFilmPanier(i)
  }

  getMoviePanier(): MovieResponse[] {
    return this.p.getMoviePanier()
  }

  getCout(): number {
    return Math.round(this.p.getCout() * 100) / 100
  }

  taillePanierClient() {
    return this.p.taillePanier()
  }
  confirmePanier() {
    this.mcv.validationCommande()
  }

  ngOnInit(): void {}

  getNomFilm(film: MovieResponse) {
    return film.title
  }

  panierNonVide(): boolean {
    return this.p.panierNonVide()
  }
}
