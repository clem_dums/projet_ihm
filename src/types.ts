export interface Commande {
  date: string
  idCommande: string
  idsFilms: string[] //Liste d'id de Films
  prix: string //Cout de la commande
  adresse: string //Adrersse de livraison
}

export interface Users {
  idClient: string
  nom: string
  prenom: string
  photoURL: string
  mail: string
  tel: string
}

export interface Utilisateur {
  idClient: string
  nom: string
  prenom: string
  photoURL: string
  mail: string
  tel: string
  nbTotalCommande: string
  derniereCommande: Commande
}

export interface Adresse {
  rue: string
  ville: string
  tel: string
  pays: string
  codeP: string
}
